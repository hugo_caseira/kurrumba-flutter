@echo off
set /p name="Enter project name: "
IF [%name%] == [] (
	echo Invalid project name.
	echo usage: "create my_project"
	echo:
	pause
	exit /b
)

cd ..
flutter create %name% & cd %name% & flutter pub add lowder --path=../lowder-flutter & ..\lowder-flutter\copy_templates.bat %name% & start_editor.bat
